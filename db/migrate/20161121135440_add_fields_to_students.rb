class AddFieldsToStudents < ActiveRecord::Migration[5.0]
  def change
    add_column :students, :name, :string
    add_column :students, :whatsapp, :string
    add_column :students, :picture_id, :integer
  end
end
