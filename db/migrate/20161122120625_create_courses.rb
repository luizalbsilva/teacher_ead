class CreateCourses < ActiveRecord::Migration[5.0]
  def change
    create_table :courses do |t|
      t.string :name
      t.string :note
      t.integer :student_id

      t.timestamps
    end
  end
end
