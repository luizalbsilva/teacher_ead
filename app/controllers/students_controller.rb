class StudentsController < ApplicationController

	def index
		@students = Student.all

		respond_to do |format|
			format.json { render json: @students.as_json(include: :courses) }
			format.html
		end
	end

	def show
		@student = Student.find params[:id]

		respond_to do |format|
			format.json { render json: @student.as_json(include: :courses) }
			format.html
		end
	end

end