class ActivitysController < ApplicationController
	before_action :set_activity, only: [:show, :edit, :update, :destroy]

	def index
		@activitys = Activity.all

		respond_to do |format|
			format.json { render json: @activitys.as_json }
			format.html
		end
	end

	def new
		@activity = Activity.new
	end

	def create
		@activity = Activity.new(activity_params)

		respond_to do |format|
			if @activity.save
				format.json { render json: @activity.as_json, status: :ok }
				format.html { redirect_to root_path }
			else
				format.json { render json: @activity.errors, status: :no_content }
				format.html { render :new }
			end
		end
	end

	def show
		@activity = Activity.find params[:id]

		respond_to do |format|
			format.json { render json: @activity.as_json }
			format.html 
		end
	end

	def edit
		@activity = Activity.find params[:id]
	end

	def update
		respond_to do |format|
			if @activity.update_attributes(activity_params)
				format.json { render json: @activity.as_json, status: :ok }
				format.html { redirect_to root_path }
			else
				format.json { render json: @activity.errors, status: :no_content }
				format.html { render :edit }
			end
		end
	end

	def destroy
		@activity.destroy

		respond_to do |format|
			format.json { render json: {status: :ok} }
			format.html { redirect_to root_path }
		end
	end

	private

		def activity_params
			params.require(:activity).permit(
				:matter, :task, 
				:dt_start, :dt_end, 
				:concept, :semester_id);
		end

		def set_activity
			@activity = Activity.find params[:id]
		end

end