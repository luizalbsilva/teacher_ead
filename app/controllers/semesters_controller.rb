class SemestersController < ApplicationController
	before_action :set_course, only: [:show, :edit, :update, :destroy]

	def index
		@semesters = Semester.all
		@semester = Semester.new

		respond_to do |format|
			format.json { render json: @semesters.as_json }
			format.html
		end
	end

	def new
		@semester = Semester.new
	end

	def create
		@semester = Semester.new(semester_params)

		respond_to do |format|
			if @semester.save
				format.json { render json: @semester.as_json, status: :ok }
				format.html { redirect_to course_path(@semester.course_id) }
			else
				format.json { render json: @semester.errors, status: :no_content }
				format.html { render :new }
			end
		end
	end

	private

		def set_semester
 			@semester = Semester.find params[:id]
		end

		def semester_params
			params.require(:semester).permit(:name, :course_id)
		end

end