class CoursesController < ApplicationController
	before_action :set_course, only: [:show, :edit, :update, :destroy]

	def index
		@courses = Course.all.reverse
		@course = Course.new

		respond_to do |format|
			format.json { render json: @courses.as_json }
			format.html
		end
	end

	def new
		@course = Course.new
	end

	def create
		@course = Course.new(course_params)
		@course.student_id = current_student.id

		respond_to do |format|
			if @course.save
				format.json { render json: @course.as_json, status: :ok }
				format.html { redirect_to courses_path }
			else
				format.json { render json: @course.errors, status: :no_content }
				format.html { render :new }
			end
		end
	end

	def destroy
		@course.destroy

		respond_to do |format|
			format.json { render json: {status: :ok} }
			format.html { redirect_to root_path }
		end
	end 

	def show
		@semester = Semester.new
		@course = Course.find params[:id]

		respond_to do |format|
			format.json { render json: @course.as_json }
			format.html 
		end
	end

	def edit
		@course = Course.find params[:id]
	end

	def update
		respond_to do |format|
			if @course.update_attributes(course_params)
				format.json { render json: @course.as_json, status: :ok }
				format.html { redirect_to courses_path }
			else
				format.json { render json: @course.errors, status: :no_content }
				render.html { render :edit }
			end
		end
	end

	private

		def set_course
			@course = Course.find params[:id]
		end

		def course_params
			params.require(:course).permit(:name, :note, :student_id)
		end

end