module ApplicationHelper

	def nav_content
		if current_student
			render "layouts/nav_with_student"
		else
			render "layouts/nav_without_student"
		end
	end

end