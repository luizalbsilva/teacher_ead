$url_courses = "http://localhost:3000/courses.json";
$url_course = "http://localhost:3000/courses/";

$url_semesters = "http://localhost:3000/semesters.json";
$url_semester = "http://localhost:3000/semesters/";

$url_activitys = "http://localhost:3000/activitys.json";
$url_activity = "http://localhost:3000/activitys/";

angular.module("EADT", []);
angular.module("EADT").controller("EADTCtrl", function($scope, $http){

	var load_courses = function(){
		$http.get($url_courses).success(function(data){
			$scope.coursesDB = data;
			console.log("COURSES loaded!");
		}).error(function(error){
			console.log("COURSES error!");
		})
	}

	var load_semesters = function(){
		$http.get($url_semesters).success(function(data){
			$scope.semestersDB = data;
			console.log("SEMESTERS loaded!");
		}).error(function(error){
			console.log("SEMESTERS error!");
		})
	}

	var load_activitys = function(){
		$http.get($url_activitys).success(function(data){
			$scope.activitysDB = data;
			console.log("ACITIVITYS loaded!");
		}).error(function(error){
			console.log("ACITIVITYS error!");
		});
	}

	$scope.create_course = function(course){
		$http.post($url_courses, course).success(function(data){
			console.log("COURSE created!");
			delete $scope.course;
			load_courses();
		}).error(function(error){
			console.log("COURSE not created!");
		})
	}

	$scope.create_semester = function(semester){
		$http.post($url_semesters, semester).success(function(data){
			console.log("SEMESTER created!");
			delete $scope.semester;
			load_semesters();
		}).error(function(error){
			console.log("SEMESTER not created!");
		})
	}

	$scope.create_activity = function(activity){
		$http.post($url_activitys, activity).success(function(data){
			console.log("ACTIVITY created!");
			delete $scope.activity;
			load_activitys();
		}).error(function(error){
			console.log("ACTIVITY not created!");
		})
	}

	$scope.delete_course = function(course){
		$http.delete($url_course + course.id + ".json", course).success(function(data){
			console.log("COURSE deleted!");
			load_courses();
		}).error(function(error){
			console.log("COURSE not deleted!");
		});
	}

	$scope.delete_semester = function(semester){
		$http.delete($url_semester + semester.id + ".json", semester).success(function(data){
			console.log("SEMESTER deleted!");
			load_semesters();
		}).error(function(error){
			console.log("SEMESTER not deleted!");
		});
	}

	$scope.delete_activity = function(activity){
		$http.delete($url_activity + activity.id + ".json", activity).success(function(data){
			console.log("ACTIVITY deleted!");
			load_activitys();
		}).error(function(error){
			console.log("ACTIVITY not deleted!");
		})
	}

	$scope.edit_course = function(course){
		course.editable = true;
	}

	$scope.update_course = function(course){
		$http.put($url_course + course.id + ".json", course).success(function(data){
			course.editable = false;
			load_courses();
			console.log("COURSE edited!");
		}).error(function(error){
			console.log("COURSE not edited!");
		});
	}

	$scope.update_activity = function(activity){
		$http.put($url_activity + activity.id + ".json", activity).success(function(data){
			load_activitys();
			console.log("ACTIVITY edited!");
		}).error(function(error){
			console.log("ACTIVITY not edited!");
		})
	}

	load_courses();
	load_semesters();
	load_activitys();

});